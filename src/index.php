<!DOCTYPE html>

<!-- PLEASE READ THE README -->

<html lang="de">

    <head>
        <!-- Set encoding to UTF-8 -->
        <meta charset="utf-8">
        
        <title>Phone number extractor</title>

        <!-- Load Bootstrap. -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        
        
        <!-- Load stylesheet -->
        <link rel="stylesheet" type="text/css" href="stylesheet.css" media="screen" />
    </head>

    <body>

        <div class="container">

            <h1>Phone number extractor</h1>

            <h5>Warning! Very case sensitive and optimized for sipgate.de/impressum</h5><br><br>

            <form>

                <div class="form-group">

                    <!-- Input field -->
                    <h4>Only full URL like : https://www.sipgate.de/impressum</h4>
                    <input type="text" class="form-control" name="url" id="url" placeholder="https://www.sipgate.de/impressum">

                    <h5>Max. 10 results for one request !</h5><br>
                    
                    <button type="submit" class="btn btn-primary" id="button" >Search</button><br><br><br>

                    <?php

                    //* Display errors//
                    //error_reporting(E_ALL);
                    //sini_set('display_errors', 1);
                     
                    /*Initializes cURL only if there is a value in $url  */
                    if(!empty($_GET['url'])) {

                        /* Initialize cURL handler */
                        $ch = curl_init();

                        /* cURL settings */
                        curl_setopt($ch, CURLOPT_URL, $_GET['url']);
                        
                        /*cURL option for successful HTTPS requests */
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        /* Returns content as string */
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        /* Safe content in variable $result */
                        $result=curl_exec($ch);

                        /*Filter for phone number and store it in $array */
                        preg_match_all("/[+49]{3} [0-9]{3} [0-9]{6}\-[0]/", $result, $array);
                        
                        /*If no matches then go to next RegEx.
                          You can use as many as you want for every kind of case.
                          This is just an example and is optimized for sipgate.de/impressum.
                          Unfortunatly i didn´t find one that works with every phone number*/
                          
                        if(empty($array)){

                            preg_match_all("/[0-9]{3} [0-9]{6}\-[0]/", $result, $array);

                            if(empty($array)){
                                preg_match_all("/0 [0-9]{3} [0-9]{6}\-[0]/", $result, $array);
                            }
                        }
                        /*End cURL session */
                        curl_close($ch);

                        /*If content in $array show content */
                        if (!empty($array[0])){

                            echo '<h3 id="hl1">Results</h3><br>';

                            echo'<ol id="resultTable">';
                            
                            if (!empty($array[0][0])) {
                                 echo '<ul>'.($array[0][0]).'</ul><br>' ;
                            }
                            if (!empty($array[0][1])) {
                                 echo '<ul>'.($array[0][1]).'</ul><br>' ;
                            }
                            if (!empty($array[0][2])) {
                                echo '<ul>'.($array[0][2]).'</ul><br>' ;
                            }
                            if (!empty($array[0][3])) {
                                 echo '<ul>'.($array[0][3]).'</ul><br>' ;
                            }
                            if (!empty($array[0][4])) {
                                 echo '<ul>'.($array[0][4]).'</ul><br>' ;
                            }
                            if (!empty($array[0][5])) {
                                 echo '<ul>'.($array[0][5]).'</ul><br>' ;
                            }
                            if (!empty($array[0][6])) {
                                 echo '<ul>'.($array[0][6]).'</ul><br>' ;
                            }
                            if (!empty($array[0][7])) {
                                 echo '<ul>'.($array[0][7]).'</ul><br>' ;
                            }
                            if (!empty($array[0][8])) {
                                 echo '<ul>'.($array[0][8]).'</ul><br>' ;
                            }
                            if (!empty($array[0][9])) {
                                 echo '<ul>'.($array[0][9]).'</ul><br>' ;
                            }
                            if (!empty($array[0][10])) {
                                 echo '<ul>'.($array[0][10]).'</ul><br>' ;
                            }
                            echo"</ol>";
                        }
                            /*If no content in $array */
                            else {
                                echo "<h3>No phone Number here, sorry!</h3>";
                            }
                        }
                    ?>
                </div>
            </form>
        </div>
    </body>
</html>
